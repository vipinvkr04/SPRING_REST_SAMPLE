package com.example.demo.service.impl;


import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.example.demo.io.entity.AddressEntity;
import com.example.demo.io.entity.UserEntity;
import com.example.demo.shared.dto.AddressDTO;
import com.example.demo.shared.dto.UserDto;
import com.example.demo.shared.utils.Utils;
import com.example.demo.ui.model.response.UserRest;

class UserServiceImplTest {
	@InjectMocks
	UserServiceImpl userServiceImpl;
	@Mock
	com.example.demo.repository.UserRepository userRepository;
	@Mock
	BCryptPasswordEncoder bCryptPasswordEncoder;
	@Mock
	Utils utils;
	UserEntity userEntity;
	String userID="hhggrty";
	String encryptedPassword="ancdefgh";
	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		userEntity=new UserEntity();
		userEntity.setId(1L);
		userEntity.setUserId(userID);
		userEntity.setFirstName("vipin");
		userEntity.setLastName("kr");
		userEntity.setEncryptedPassword(encryptedPassword);
		userEntity.setAddresses(getAddressEntity());
	}

	@Test
	@Disabled
	void getUserTest() {
		when(userRepository.findByEmail(anyString())).thenReturn(userEntity);
		UserDto userdto=userServiceImpl.getUser("test@test.com");
		assertNotNull(userdto);
		assertEquals(userEntity.getFirstName(), userdto.getFirstName());
	}
	@Test
	@Disabled
	void getUserUsernameNotFoundExceptionTest() {
		when(userRepository.findByEmail(anyString())).thenReturn(null);
		assertThrows(UsernameNotFoundException.class, ()->{
			userServiceImpl.getUser("test@test.com");
		
		});
	}
	@Test
	void createUserTest() {
		when(userRepository.findByEmail(anyString())).thenReturn(null);
		when(userRepository.save(any(UserEntity.class))).thenReturn(userEntity);
		when(utils.generateAddressId(anyInt())).thenReturn("ancdefgh");
		when(utils.generateUserId(anyInt())).thenReturn("hhggrty");
		
		UserDto userDto=new UserDto();
		userDto.setAddresses(getAddressDto());
		UserDto storedUserDetails=userServiceImpl.createUser(userDto);
		assertNotNull(storedUserDetails);
		assertEquals(userEntity.getFirstName(), storedUserDetails.getFirstName());
		assertEquals(storedUserDetails.getAddresses().size(),1);
	}
   
	private List<AddressDTO> getAddressDto(){
		AddressDTO shippingAddress=new AddressDTO();
		shippingAddress.setType("shipping");
		List<AddressDTO> addresses=new ArrayList<AddressDTO>();
		addresses.add(shippingAddress);
		return addresses;
	}
	private List<AddressEntity> getAddressEntity(){
		List<AddressDTO> addresses = getAddressDto();
		Type listType = new TypeToken<List<AddressEntity>>() {
		}.getType();
		return new ModelMapper().map(addresses, listType);
	}
}
