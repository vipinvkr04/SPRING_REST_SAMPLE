package com.example.demo.ui.controller;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;

import com.example.demo.exception.UserServiceException;
import com.example.demo.service.AddressService;
import com.example.demo.service.UserService;
import com.example.demo.shared.dto.AddressDTO;
import com.example.demo.shared.dto.UserDto;
import com.example.demo.ui.model.request.RequestOperationName;
import com.example.demo.ui.model.request.UserDetailsRequestModel;
import com.example.demo.ui.model.response.AddressesRest;
import com.example.demo.ui.model.response.ErrorMessages;
import com.example.demo.ui.model.response.OperationStatusModel;
import com.example.demo.ui.model.response.RequestOperationStatus;
import com.example.demo.ui.model.response.UserRest;

@RestController
@RequestMapping("users")
public class UserController {
	@Autowired
	UserService userService;
    @Autowired
    AddressService addressesService;
	@GetMapping(path = "/{id}", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public UserRest getUser(@PathVariable String id) {
		UserRest returnValue = new UserRest();

		UserDto userDto = userService.getUserByUserId(id);
		
		  ModelMapper modelMapper = new ModelMapper(); 
		  returnValue=modelMapper.map(userDto, UserRest.class);
		 
	//	BeanUtils.copyProperties(userDto, returnValue);
		return returnValue;
	}

	@PostMapping(consumes = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public UserRest CreateUser(@RequestBody UserDetailsRequestModel userDetails) throws Exception {
		UserRest returnValue = new UserRest();
		if (userDetails.getFirstName().isEmpty()) {
			throw new NullPointerException(ErrorMessages.MISSING_REQUIRED_FIELD.getErrorMessage());
		}
		
		ModelMapper modelMapper=new ModelMapper();
		UserDto userDto = modelMapper.map(userDetails, UserDto.class);
		//BeanUtils.copyProperties(userDetails, userDto);
	
		UserDto createdUser = userService.createUser(userDto);
		returnValue=modelMapper.map(createdUser, UserRest.class);
		// BeanUtils.copyProperties(createdUser, returnValue);
		return returnValue;

	}

	@PutMapping(path = "/{id}", consumes = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })

	public UserRest updateUser(@PathVariable String id, @RequestBody UserDetailsRequestModel userDetails) {
		UserRest returnValue = new UserRest();

		//UserDto userDto = new UserDto();
		ModelMapper modelMapper=new ModelMapper();
		UserDto userDto= modelMapper.map(userDetails, UserDto.class);
		//BeanUtils.copyProperties(userDetails, userDto);
		UserDto updateUser = userService.updateUser(id, userDto);
		returnValue=modelMapper.map(updateUser, UserRest.class);
		//BeanUtils.copyProperties(updateUser, returnValue);
		return returnValue;
	}

	@DeleteMapping(path = "/{id}", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public OperationStatusModel deleteUser(@PathVariable String id) {
		OperationStatusModel returnValue = new OperationStatusModel();
		returnValue.setOperationName(RequestOperationName.DELETE.name());
		userService.deleteUser(id);
		returnValue.setOperationResult(RequestOperationStatus.SUCCESS.name());
		return returnValue;
	}
	
	// get list of addresses
	@GetMapping(path = "/{id}/addresses", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public List<AddressesRest> getUserAddresses(@PathVariable String id) {
		List<AddressesRest> addressesListRestModel = new ArrayList<>();
		List<AddressDTO> addressesDTO = addressesService.getAddresses(id);
		if (addressesDTO != null && !addressesDTO.isEmpty()) {
			Type listType = new TypeToken<List<AddressesRest>>(){}.getType();
			addressesListRestModel = new ModelMapper().map(addressesDTO, listType);
		}
		return addressesListRestModel;
	}
	// get user address
		@GetMapping(path = "/{id}/address/{addressId}", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
		public AddressesRest getUserAddress(@PathVariable String id,@PathVariable String addressId) {
			AddressesRest returnValue=null;
			AddressDTO addressDto=addressesService.getAddress(addressId);
			if(addressDto!=null) {
				returnValue=new ModelMapper().map(addressDto, AddressesRest.class);
			}
			return returnValue;
		}
		
	//get users pagination
		public List<UserRest> getUsers(@RequestParam(value = "page", defaultValue = "0") int page,
				@RequestParam(value = "limit", defaultValue = "2") int limit) {
			List<UserRest> returnValue = new ArrayList<>();

			List<UserDto> users = userService.getUsers(page, limit);
			
			Type listType = new TypeToken<List<UserRest>>() {
			}.getType();
			returnValue = new ModelMapper().map(users, listType);

			/*for (UserDto userDto : users) {
				UserRest userModel = new UserRest();
				BeanUtils.copyProperties(userDto, userModel);
				returnValue.add(userModel);
			}*/

			return returnValue;
		}
}
