package com.example.demo.service.impl;

import java.awt.print.Pageable;
import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.example.demo.exception.UserServiceException;
import com.example.demo.io.entity.UserEntity;
import com.example.demo.service.UserService;
import com.example.demo.shared.dto.AddressDTO;
import com.example.demo.shared.dto.UserDto;
import com.example.demo.shared.utils.Utils;
import com.example.demo.ui.model.response.ErrorMessages;
@Service
@Transactional
public class UserServiceImpl implements UserService {
	@Autowired
	com.example.demo.repository.UserRepository userRepository;
	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;
	@Autowired
	Utils utils;
	@Override
	public UserDto createUser(UserDto user) {
		
		if(userRepository.findByEmail(user.getEmail())!=null) {
			throw new RuntimeException("Record already exist");
		}
		for(int i=0;i<user.getAddresses().size();i++)
		{
			AddressDTO address = user.getAddresses().get(i);
			address.setUserDetails(user);
			address.setAddressId(utils.generateAddressId(30));
			user.getAddresses().set(i, address);
		}
		/*
		UserEntity userEntity=new UserEntity();
		BeanUtils.copyProperties(user, userEntity);
		*/
		 ModelMapper modelMapper = new ModelMapper(); 
		 UserEntity userEntity=modelMapper.map(user, UserEntity.class);
		String publicUserId = utils.generateUserId(30);
		userEntity.setUserId(publicUserId);
		userEntity.setEncryptedPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		UserEntity storedUserDetails=userRepository.save(userEntity);
		//UserDto returnValue=new UserDto();
		//BeanUtils.copyProperties(storedUserDetails,returnValue);
		 UserDto returnValue=modelMapper.map(storedUserDetails, UserDto.class);
		return returnValue;
	}
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		UserEntity userEntity = userRepository.findByEmail(email);

		if (userEntity == null)
			throw new UsernameNotFoundException(email);
		
		return new User(userEntity.getEmail(), userEntity.getEncryptedPassword(),new ArrayList<>());

	}
	@Override
	public UserDto getUser(String userName) {
		UserEntity userEntity=userRepository.findByEmail(userName);
		if(userEntity==null) {
			throw new UsernameNotFoundException(userName);
		}
		/*
		UserDto userDto=new UserDto();
		BeanUtils.copyProperties(userEntity, userDto);
		*/
		 ModelMapper modelMapper = new ModelMapper(); 
		 UserDto userDto=modelMapper.map(userEntity, UserDto.class);
		return userDto;
	}
	@Override
	public UserDto getUserByUserId(String userId) {
		//UserDto returnValue = new UserDto();
		UserEntity userEntity = userRepository.findByUserId(userId);

		if (userEntity == null)
			throw new UsernameNotFoundException("User with ID: " + userId + " not found");

		 ModelMapper modelMapper = new ModelMapper(); 
		 UserDto returnValue=modelMapper.map(userEntity, UserDto.class);
		return returnValue;
	}
	@Override
	public UserDto updateUser(String userId, UserDto user) {
		//UserDto returnValue = new UserDto();

		UserEntity userEntity = userRepository.findByUserId(userId);

		if (userEntity == null)
			throw new UserServiceException(ErrorMessages.NO_RECORD_FOUND.getErrorMessage());

		userEntity.setFirstName(user.getFirstName());
		userEntity.setLastName(user.getLastName());

		UserEntity updatedUserDetails = userRepository.save(userEntity);
		//returnValue = new ModelMapper().map(updatedUserDetails, UserDto.class);
		 ModelMapper modelMapper = new ModelMapper(); 
		 UserDto returnValue=modelMapper.map(updatedUserDetails, UserDto.class);
	//	BeanUtils.copyProperties(updatedUserDetails, returnValue);
		return returnValue;
	}
	@Transactional
	@Override
	public void deleteUser(String userId) {
		UserEntity userEntity = userRepository.findByUserId(userId);

		if (userEntity == null)
			//throw new UserServiceException(ErrorMessages.NO_RECORD_FOUND.getErrorMessage());

		userRepository.delete(userEntity);

	}
	//pagination
	@Override
	public List<UserDto> getUsers(int page, int limit) {
List<UserDto> returnValue = new ArrayList<>();
		
		if(page>0) page = page-1;
		
		Pageable pageableRequest = (Pageable) PageRequest.of(page, limit);
		
		Page<UserEntity> usersPage = userRepository.findAll(pageableRequest);
		List<UserEntity> users = usersPage.getContent();
		
        for (UserEntity userEntity : users) {
            UserDto userDto = new UserDto();
            BeanUtils.copyProperties(userEntity, userDto);
            returnValue.add(userDto);
        }
		
		return returnValue;
	}

}
