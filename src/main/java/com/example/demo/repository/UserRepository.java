package com.example.demo.repository;

import java.awt.print.Pageable;

import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.io.entity.UserEntity;

@Repository
public interface UserRepository extends JpaRepository<UserEntity,Long>{

	UserEntity findByEmail(String email);

	UserEntity findByUserId(String userId);

	Page<UserEntity> findAll(Pageable pageableRequest);

	
}
